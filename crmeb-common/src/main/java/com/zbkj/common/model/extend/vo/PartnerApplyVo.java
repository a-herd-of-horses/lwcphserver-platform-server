package com.zbkj.common.model.extend.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class PartnerApplyVo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "申请人ID")
    private Long applicantId;

    @ApiModelProperty(value = "审批人ID")
    private Long approverId;

    @ApiModelProperty(value = "申请日期")
    private Date applicantDate;

    @ApiModelProperty(value = "审批日期")
    private Date approverDate;

    @ApiModelProperty(value = "审批状态 0不通过 1通过")
    private Integer approverState;

    @ApiModelProperty(value = "申请合伙人等级")
    private Integer applicantLevel;

    @ApiModelProperty(value = "申请人真实姓名")
    private String applicantRealName;

    @ApiModelProperty(value = "审批人真实姓名")
    private String approverRealName;

    @ApiModelProperty(value = "行政区划全称")
    private String mergerName;

}
