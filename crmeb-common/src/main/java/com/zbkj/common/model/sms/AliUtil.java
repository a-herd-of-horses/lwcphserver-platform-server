package com.zbkj.common.model.sms;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.zbkj.common.utils.CrmebUtil;


import java.util.Random;

public class AliUtil {

    private static IAcsClient getClient(String accessKeyId,String secret){
        DefaultProfile profile = DefaultProfile.getProfile(FianlDataManager.getRegionId(),
               accessKeyId
                , secret);
        return new DefaultAcsClient(profile);
    }

    public static CommonResponse sendMSM(String phoneNumber, CommonRequest request, String accessKeyId,
                                 String secret, String templateCode, String signName, Integer code) {
        IAcsClient clients  = getClient(accessKeyId,secret);
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumber);
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", "{\"code\":"+code+"}");
        try {
            return clients.getCommonResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }




}
