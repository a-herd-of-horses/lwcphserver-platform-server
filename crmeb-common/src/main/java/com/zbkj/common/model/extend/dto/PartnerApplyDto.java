package com.zbkj.common.model.extend.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class PartnerApplyDto implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "申请人ID")
    private Long applicantId;

    @ApiModelProperty(value = "审批人ID")
    private Integer approverId;

    @ApiModelProperty(value = "申请等级")
    private Integer applicantLevel;

    @ApiModelProperty(value = "区域编码")
    private String areaCode;


}
