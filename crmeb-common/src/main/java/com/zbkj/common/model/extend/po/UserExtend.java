package com.zbkj.common.model.extend.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("td_user_extend")
@ApiModel(value="UserExtend对象", description="用户推广关系表")
public class UserExtend {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    @TableField("uid")
    @ApiModelProperty(value = "用户id")
    private Long uid;

    @TableField("parent_uid")
    @ApiModelProperty(value = "上级用户id")
    private Long parentUid;

    @TableField("is_partner")
    @ApiModelProperty(value = "是否是合伙人")
    private Integer isPartner;

    @TableField("has_buy_499")
    @ApiModelProperty(value = "是否购买过499元商品")
    private Integer hasBuy499;

    @TableField("goods_voucher")
    @ApiModelProperty(value = "提货券")
    private Integer goodsVoucher;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "上级用户主键")
    private Long pId;

}
