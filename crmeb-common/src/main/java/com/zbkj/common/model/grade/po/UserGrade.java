package com.zbkj.common.model.grade.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("td_user_grade")
@ApiModel(value="UserGrade对象", description="五级合伙人关系表")
public class UserGrade {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    @TableField("uid")
    @ApiModelProperty(value = "用户id")
    private Long uid;

    @TableField("parent_uid")
    @ApiModelProperty(value = "上级用户id")
    private Long parentUid;

    @TableField("level")
    @ApiModelProperty(value = "五级合伙人：0普通用户、1社区、2街道/镇、3区/县、4市、5省")
    private Long level;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "上级用户主键")
    private Long pId;

}
