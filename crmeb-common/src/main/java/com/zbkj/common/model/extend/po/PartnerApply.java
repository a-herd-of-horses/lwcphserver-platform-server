package com.zbkj.common.model.extend.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("td_partner_apply")
@ApiModel(value="PartnerApply对象", description="五级合伙人申请表")
public class PartnerApply {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    @TableField("applicant_id")
    @ApiModelProperty(value = "申请人ID")
    private Long applicantId;

    @TableField("approver_id")
    @ApiModelProperty(value = "审批人ID")
    private Long approverId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@TableField(value = "applicant_date", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "申请日期")
    private Date applicantDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@TableField(value = "approver_date", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "审批日期")
    private Date approverDate;

    @TableField("approver_state")
    @ApiModelProperty(value = "审批状态 0不通过 1通过")
    private Integer approverState;

    @TableField("applicant_level")
    @ApiModelProperty(value = "申请等级")
    private Integer applicantLevel;

    @TableField("area_code")
    @ApiModelProperty(value = "区域编码")
    private String areaCode;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


}
