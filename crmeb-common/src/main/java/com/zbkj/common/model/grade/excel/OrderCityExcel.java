package com.zbkj.common.model.grade.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="OrderCityExcel对象", description="五级城市Excel表")
public class OrderCityExcel implements Serializable {

    private static final long serialVersionUID=1L;

//    @ExcelProperty("省份")
//    @ApiModelProperty(value = "省份")
//    private String provinceName;
//
//    @ExcelProperty("城市代码")
//    @ApiModelProperty(value = "城市代码")
//    private String cityCode;
//
//    @ExcelProperty("城市")
//    @ApiModelProperty(value = "城市")
//    private String cityName;

//    @ExcelProperty("区县代码")
//    @ApiModelProperty(value = "区县代码")
//    private String areaCode;

//    @ExcelProperty("区县")
//    @ApiModelProperty(value = "区县")
//    private String areaName;
//
    @ExcelProperty("乡镇街道代码")
    @ApiModelProperty(value = "乡镇街道代码")
    private String streetCode;

//    @ExcelProperty("乡镇街道")
//    @ApiModelProperty(value = "乡镇街道")
//    private String streetName;
//
    @ExcelProperty("居委会村代码")
    @ApiModelProperty(value = "居委会村代码")
    private String communityCode;
//
//    @ExcelProperty("城乡分类代码")
//    @ApiModelProperty(value = "城乡分类代码")
//    private String typeCode;
//
    @ExcelProperty("居委会村")
    @ApiModelProperty(value = "居委会村")
    private String communityName;
//
//    @ExcelProperty("地址")
//    @ApiModelProperty(value = "地址")
//    private String address;

}
