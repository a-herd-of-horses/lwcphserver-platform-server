package com.zbkj.common.model.extend.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserExtendDto implements Serializable {

    private static final long serialVersionUID=1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键")
    private Long id;

    @NotNull(message = "用户id不能为空")
    @ApiModelProperty(value = "用户id")
    private Long uid;

    @NotNull(message = "被推广用户不能为空!")
    @ApiModelProperty(value = "被推广用户id")
    private Long extendUid;

    @ApiModelProperty(value = "是否是合伙人 1表示是 0表示否")
    private Integer isPartner;

    @ApiModelProperty(value = "是否购买499元商品")
    private Integer hasBuy499;

    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户账号")
    private String account;

    private Integer width;

    private Integer height;

}
