package com.zbkj.common.model.grade.po;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("td_grade_city")
@ApiModel(value="OrderCity对象", description="五级城市表")
public class OrderCity implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "区号")
    @TableField("area_code")
    private String areaCode;

    @ApiModelProperty(value = "父级区号")
    @TableField("parent_code")
    private String parentCode;

    @ApiModelProperty(value = "省市级别")
    @TableField("level")
    private Integer level;

    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "合并名称")
    @TableField("merger_name")
    private String mergerName;

    @ApiModelProperty(value = "经度")
    @TableField("lng")
    private String lng;

    @ApiModelProperty(value = "纬度")
    @TableField("lat")
    private String lat;

    @ApiModelProperty(value = "是否展示")
    @TableField("is_show")
    private Boolean isShow;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
