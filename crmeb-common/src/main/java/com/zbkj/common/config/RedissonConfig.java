package com.zbkj.common.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

    @Value("${redisson.address}")
    private String redisAddress;

    @Bean
    public RedissonClient redissonClient() {
        // 创建
        Config config = new Config();
        config.useSingleServer().setAddress(redisAddress);
        // 创建RedissonClient对象
        return Redisson.create(config);
    }

}
