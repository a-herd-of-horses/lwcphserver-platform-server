package com.zbkj.service.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.extend.dto.UserExtendDto;
import com.zbkj.common.model.extend.po.UserExtend;
import com.zbkj.common.model.grade.po.UserGrade;
import com.zbkj.common.model.extend.vo.UserExtendVo;
import com.zbkj.common.model.user.User;
import com.zbkj.common.utils.QRCodeUtil;
import com.zbkj.service.dao.UserExtendDao;
import com.zbkj.service.service.UserExtendService;
import com.zbkj.service.service.UserGradeService;
import com.zbkj.service.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.zbkj.common.constants.RedisConstatns.LOCK_EXTEND_BINDING_KEY;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserExtendServiceImpl extends ServiceImpl<UserExtendDao, UserExtend> implements UserExtendService {

    // redissoon
    private final RedissonClient redissonClient;

    // 用户信息
    private final UserService userService;

    // 五级合伙人
    private final UserGradeService userGradeService;

    // mapper
    private final UserExtendDao userExtendDao;

    // 代理对象
    private UserExtendService proxy;

    /**
     * 生成用户推广二维码
     * @param userExtendDto
     * @return
     */
    @Override
    public String generateExtendCode(UserExtendDto userExtendDto) {

        Long uid = userExtendDto.getUid(); //用户id
        Integer width = userExtendDto.getWidth(); //二维码宽
        Integer height = userExtendDto.getHeight(); //二维码高

        String code = null;
        try {
            // 默认长200 宽200
            width = width == null ? 200 : width;
            height = height == null ? 200 : height;
            // 生成二维码
            code = QRCodeUtil.crateQRCode(uid.toString(), width, height);
        } catch (IOException e) {
            throw new RuntimeException("生成推广码异常");
        }
        return code;
    }

    /**
     * 绑定上下级关系
     * @param userExtendDto
     */
    @Override
    public void bindingUser(UserExtendDto userExtendDto) {

        // 用户id
        Long uid = userExtendDto.getUid();
        // 被推广用户
        Long extendUid = userExtendDto.getExtendUid();

        // 推广用户锁
        RLock userLock = redissonClient.getLock(LOCK_EXTEND_BINDING_KEY + uid);
        // 被推广用户锁
        RLock userExtendLock = redissonClient.getLock(LOCK_EXTEND_BINDING_KEY + extendUid);

        // 多个锁一起获取
        RLock multiLock = redissonClient.getMultiLock(userLock, userExtendLock);

        // 获取锁
        boolean flag = false;
        try {
            flag = multiLock.tryLock(10, 30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error("获取锁异常");
        }

        // 获取锁失败
        if(!flag) {
            throw new RuntimeException("系统繁忙，请稍后再试!");
        }

        // 获取代理对象（事务）
        proxy = (UserExtendService) AopContext.currentProxy();

        try {
            // 绑定推广关系
            proxy.binding(uid, extendUid);
        } finally {
            //释放锁
            multiLock.unlock();
        }
    }

    /**
     * 绑定2+1推广关系 和 五级合伙人关系
     * @param uid 推广用户
     * @param extendUid 被推广用户
     */
    @Transactional
    public void binding(Long uid, Long extendUid) {

        Integer userCount = userService.lambdaQuery()
                .eq(User::getUid, uid)
                .or()
                .eq(User::getUid, extendUid)
                .count();

        if(userCount != 2) {
            throw new RuntimeException("用户不存在!");
        }

        // 查询被推广用户
        UserExtend userExtend = lambdaQuery().eq(UserExtend::getUid, extendUid).one();

        // 不为空，已绑定推广关系
        if(ObjectUtil.isNotEmpty(userExtend)) {
            throw new RuntimeException("用户已绑定推广关系!");
        }

        // 推广用户
        UserExtend user = lambdaQuery().eq(UserExtend::getUid, uid).one();
        // 关联的下级用户
        List<UserExtend> userExtendList = Collections.emptyList();
        // 如果推广用户不存在，则创建
        if(ObjectUtil.isEmpty(user)) {
            user = new UserExtend();
            user.setUid(uid);
            user.setParentUid(0L);
            save(user);
        } else {
            userExtendList = lambdaQuery()
                    .eq(UserExtend::getParentUid, uid)
                    .eq(UserExtend::getHasBuy499, 1)
                    .list();
        }

        // 被推广用户
        userExtend = new UserExtend();
        userExtend.setUid(extendUid);
        userExtend.setParentUid(uid);

        // 判断关联的三级用户数量，如果大于 >2
        if(userExtendList.size() >= 2) {

            for (UserExtend item : userExtendList) {
                // 间接上级转为直接上级
                item.setParentUid(user.getParentUid());
            }

            // 推广用户变为合伙人
            user.setIsPartner(1);
            userExtendList.add(user);

            // 修改
            updateBatchById(userExtendList);
        }

        // 保存被推广用户
        save(userExtend);

        // 判断是否已经绑定五级合伙人关系
        UserGrade grade = userGradeService.lambdaQuery().eq(UserGrade::getUid, extendUid).one();

        if(ObjectUtil.isNotEmpty(grade)) {
            throw new RuntimeException("用户已绑定推广关系!");
        }

        // 判断推荐用户是否已经存在
        UserGrade userGrade = userGradeService.lambdaQuery().eq(UserGrade::getUid, uid).one();
        if(ObjectUtil.isEmpty(userGrade)) {
            // 不存在，新增
            userGrade = new UserGrade();
            userGrade.setUid(uid);
            userGrade.setParentUid(0L);
            if(userExtendList.size() >= 2) {
                userGrade.setLevel(0L);  // 普通合伙人
            }
            userGradeService.save(userGrade);
        }

        // 保存被推荐用户
        grade = new UserGrade();
        grade.setUid(extendUid);
        grade.setParentUid(uid);
        userGradeService.save(grade);
    }

    /**
     * 查询链动2+1树
     * @param userExtendDto
     * @return
     */
    @Override
    public List<UserExtendVo> getTree(UserExtendDto userExtendDto) {
        List<UserExtendVo> userExtendVoList = selectListByDto(userExtendDto);
        return bindTree(userExtendVoList);
    }

    @Override
    public List<UserExtendVo> selectListByDto(UserExtendDto userExtendDto) {
        return userExtendDao.selectListByDto(userExtendDto);
    }


    // 构建链动2+1树
    public List<UserExtendVo> bindTree(List<UserExtendVo> userExtendVoList) {

        List<UserExtendVo> returnList = new ArrayList<UserExtendVo>();
        List<Long> tempList = userExtendVoList.stream().map(UserExtendVo::getUid).collect(Collectors.toList());
        for (Iterator<UserExtendVo> iterator = userExtendVoList.iterator(); iterator.hasNext();)
        {
            UserExtendVo userExtendVo = (UserExtendVo) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(userExtendVo.getParentUid()))
            {
                recursionFn(userExtendVoList, userExtendVo);
                returnList.add(userExtendVo);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = userExtendVoList;
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list 分类表
     * @param t 子节点
     */
    private void recursionFn(List<UserExtendVo> list, UserExtendVo t)
    {
        // 得到子节点列表
        List<UserExtendVo> childList = getChildList(list, t);
        t.setChildren(childList);
        for (UserExtendVo tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<UserExtendVo> getChildList(List<UserExtendVo> list, UserExtendVo t)
    {
        List<UserExtendVo> tlist = new ArrayList<UserExtendVo>();
        Iterator<UserExtendVo> it = list.iterator();
        while (it.hasNext())
        {
            UserExtendVo n = (UserExtendVo) it.next();
            if (n.getParentUid().longValue() == t.getId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<UserExtendVo> list, UserExtendVo t)
    {
        return getChildList(list, t).size() > 0;
    }

    @Override
    public boolean convertPartner(Long id) {
        UserExtend currMember = getById(id);
        //查询所有购买过499的下级会员
        QueryWrapper<UserExtend> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_uid", currMember.getParentUid());
        queryWrapper.eq("has_buy_499", 1);
        List<UserExtend> memberList = list(queryWrapper);
        if (memberList.size() == 2) {
            UserExtend userExtend = new UserExtend();
            userExtend.setId(currMember.getPId());
            userExtend.setIsPartner(1);
            UserGrade userGrade = new UserGrade();
            userGrade.setId(currMember.getPId());
            userGrade.setLevel(0L);
            return updateById(userExtend) && userGradeService.updateById(userGrade);
        }
        return false;
    }

    @Override
    public UserExtend getById(Serializable id) {
        UserExtend userExtend = getById(id);
        QueryWrapper<UserExtend> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid", userExtend.getParentUid());
        UserExtend result = getOne(queryWrapper);
        result.setPId(result.getId());
        return result;
    }

    /**
     * 删除链动2+1 和 五级合伙人
     * @param uid
     */
    @Override
    @Transactional
    public void removeTreeByUid(Long uid) {
        // 链动2+1
        Integer count1 = lambdaQuery().eq(UserExtend::getParentUid, uid).count();
        if(count1 != null && count1 > 0) {
            throw new CrmebException("该用户链动2+1策略有下级用户，无法删除！");
        }

        // 五级合伙人
        Integer count2 = userGradeService.lambdaQuery().eq(UserGrade::getParentUid, uid).count();
        if(count2 != null && count2 > 0) {
            throw new CrmebException("该用户五级合伙人策略有下级用户，无法删除！");
        }

        // 删除 链动2+1
        LambdaQueryWrapper<UserExtend> UserExtendWrapper = new LambdaQueryWrapper<>();
        UserExtendWrapper.eq(UserExtend::getUid, uid);
        remove(UserExtendWrapper);

        // 删除 五级合伙人
        LambdaQueryWrapper<UserGrade> UserGradeWrapper = new LambdaQueryWrapper<>();
        UserGradeWrapper.eq(UserGrade::getUid, uid);
        userGradeService.remove(UserGradeWrapper);

    }

}
