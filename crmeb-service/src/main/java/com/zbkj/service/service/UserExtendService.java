package com.zbkj.service.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.injector.methods.SelectOne;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.extend.dto.UserExtendDto;
import com.zbkj.common.model.extend.po.UserExtend;
import com.zbkj.common.model.extend.vo.UserExtendVo;
import com.zbkj.common.vo.MyRecord;

import java.util.List;

import java.io.Serializable;
import java.util.List;

public interface UserExtendService extends IService<UserExtend> {

    String generateExtendCode(UserExtendDto userExtendDt);

    void bindingUser(UserExtendDto userExtendDto);

    void binding(Long uid, Long extendUid);

    List<UserExtendVo> getTree(UserExtendDto userExtendDto);

    List<UserExtendVo> selectListByDto(UserExtendDto userExtendDto);

    boolean convertPartner(Long uid);

    void removeTreeByUid(Long uid);
}
