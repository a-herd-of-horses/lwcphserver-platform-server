package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.extend.dto.PartnerApplyDto;
import com.zbkj.common.model.extend.po.PartnerApply;
import com.zbkj.common.model.extend.vo.PartnerApplyVo;
import com.zbkj.common.model.grade.po.UserGrade;
import com.zbkj.common.model.grade.dto.UserGradeDto;
import com.zbkj.common.model.grade.vo.UserGradeVo;
import com.zbkj.common.request.PageParamRequest;

import java.util.List;

public interface UserGradeService extends IService<UserGrade> {

    List<UserGradeVo> selectListByDto(UserGradeDto userGradeDto);

    Long checkPartnerCondition(Integer id);

    boolean applyPartner(PartnerApplyDto partnerApplyDto);

    boolean approvePartner(Long id, Integer state);

    PageInfo<PartnerApplyVo> listApprovePartner(PartnerApplyDto request, PageParamRequest pageParamRequest);
}
