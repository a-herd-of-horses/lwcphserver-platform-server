package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.extend.dto.PartnerApplyDto;
import com.zbkj.common.model.extend.po.PartnerApply;
import com.zbkj.common.model.extend.vo.PartnerApplyVo;
import com.zbkj.common.model.grade.po.UserGrade;
import com.zbkj.common.model.grade.dto.UserGradeDto;
import com.zbkj.common.model.grade.vo.UserGradeVo;
import com.zbkj.common.model.product.StoreProduct;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.utils.SecurityUtil;
import com.zbkj.common.vo.LoginUserVo;
import com.zbkj.service.dao.PartnerApplyDao;
import com.zbkj.service.dao.UserGradeDao;
import com.zbkj.service.service.UserGradeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserGradeServiceImpl extends ServiceImpl<UserGradeDao, UserGrade> implements UserGradeService {

    private final UserGradeDao userGradeDao;
    private final PartnerApplyDao partnerApplyDao;

    @Override
    public List<UserGradeVo> selectListByDto(UserGradeDto userGradeDto) {
        return userGradeDao.selectListByDto(userGradeDto);
    }


    @Override
    public PageInfo<PartnerApplyVo> listApprovePartner(PartnerApplyDto request, PageParamRequest pageParamRequest) {
        Page<PartnerApplyVo> page = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        List<PartnerApplyVo> partnerApplies = partnerApplyDao.selectList();//todo 条件查询
        return CommonPage.copyPageInfo(page, partnerApplies);
    }

    @Override
    public boolean approvePartner(Long id, Integer state) {
        LoginUserVo loginUserVo = SecurityUtil.getLoginUserVo();
        PartnerApply partnerApply = new PartnerApply();
        partnerApply.setId(id);
        partnerApply.setApproverState(state);
        partnerApply.setApproverId(Long.valueOf(loginUserVo.getUser().getId()));
        partnerApply.setApproverDate(new Date());
        return partnerApplyDao.updateById(partnerApply) > 0;
    }

    @Override
    public boolean applyPartner(PartnerApplyDto partnerApplyDto) {
        PartnerApply partnerApply = new PartnerApply();
        partnerApply.setApplicantId(partnerApplyDto.getApplicantId());
        partnerApply.setApplicantLevel(partnerApplyDto.getApplicantLevel());
        partnerApply.setAreaCode(partnerApplyDto.getAreaCode());
        partnerApply.setApplicantDate(new Date());
        return partnerApplyDao.insert(partnerApply) > 0;
    }

    @Override
    public Long checkPartnerCondition(Integer id) {
        UserGrade currMember = getById(id);
        QueryWrapper<UserGrade> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_uid", currMember.getParentUid());
        List<UserGrade> partnerList = list(queryWrapper);
        Map<Long, Long> countMap = partnerList.stream()
                .collect(Collectors.groupingBy(
                        item -> item.getLevel() == null? -1 : item.getLevel(),
                        Collectors.counting()
                ));
        Map<Long, Long> filteredMap = countMap.entrySet().stream()
                .filter(entry -> {
                    Long key = entry.getKey();
                    Long value = entry.getValue();
                    //省
                    if (key == 4 && value >= 6) {
                        return true;
                    //市
                    } else if (key == 3 && value >= 4) {
                        return true;
                    //区/县
                    } else if (key == 2 && value >= 3) {
                        return true;
                    //街道/镇
                    } else if (key == 1 && value >= 2) {
                        return true;
                    //社区
                    } else if (key == 0 && value >= 2) {
                        return true;
                    }
                    return false;
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        if (filteredMap.isEmpty()) {
            return 0L;
        }
        Optional<Long> maxKey = filteredMap.keySet().stream().max(Long::compare);
        return maxKey.get();
    }

    @Override
    public UserGrade getById(Serializable uid) {
        QueryWrapper<UserGrade> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid", uid);
        UserGrade userGrade = getOne(queryWrapper);
        queryWrapper.eq("uid", userGrade.getParentUid());
        UserGrade result = getOne(queryWrapper);
        if (result != null) {
            userGrade.setPId(result.getId());
        }
        return userGrade;
    }
}
