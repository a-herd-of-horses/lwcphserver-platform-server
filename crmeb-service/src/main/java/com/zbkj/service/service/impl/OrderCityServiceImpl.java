package com.zbkj.service.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.grade.po.OrderCity;
import com.zbkj.service.dao.OrderCityDao;
import com.zbkj.service.service.*;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* ArticleServiceImpl 接口实现
*  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
*/
@Service
public class OrderCityServiceImpl extends ServiceImpl<OrderCityDao, OrderCity> implements OrderCityService {

    /**
     * 根据父级区号查询子级列表
     * @param parentCode
     * @return
     */
    @Override
    public List<OrderCity> listByPCode(String parentCode) {
        // 判断parentCode不能为空
        if (StrUtil.isBlank(parentCode)) {
            throw new CrmebException("parentCode不能为空");
        }
        // 查询
        List<OrderCity> list = lambdaQuery()
                .eq(OrderCity::getParentCode, parentCode)
                .ne(OrderCity::getAreaCode, 0)
                .list();
        return list;
    }
}

