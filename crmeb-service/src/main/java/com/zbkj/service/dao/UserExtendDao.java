package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.extend.dto.UserExtendDto;
import com.zbkj.common.model.extend.po.UserExtend;
import com.zbkj.common.model.extend.vo.UserExtendVo;

import java.util.List;

public interface UserExtendDao extends BaseMapper<UserExtend> {

    List<UserExtendVo> selectListByDto(UserExtendDto userExtendDto);

}
