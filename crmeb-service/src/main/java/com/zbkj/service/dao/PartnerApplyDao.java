package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.extend.dto.PartnerApplyDto;
import com.zbkj.common.model.extend.po.PartnerApply;
import com.zbkj.common.model.extend.vo.PartnerApplyVo;
import com.zbkj.common.model.grade.dto.UserGradeDto;
import com.zbkj.common.model.grade.vo.UserGradeVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PartnerApplyDao extends BaseMapper<PartnerApply> {

    @Select("SELECT " +
            "  a.id, " +
            "  a.applicant_date, " +
            "  a.approver_date, " +
            "  a.applicant_level, " +
            "  a.approver_state, " +
            "  u.real_name applicant_real_name, " +
            "  u2.real_name approver_real_name, " +
            "  c.merger_name  " +
            "FROM " +
            "  td_partner_apply a " +
            "  JOIN eb_system_city c ON a.area_code = c.area_code " +
            "  JOIN eb_user u ON a.applicant_id = u.uid " +
            "  LEFT JOIN eb_user u2 ON a.approver_id = u2.uid  " +
            "  AND u.STATUS = 1  " +
            "  AND u2.STATUS = 1")
    List<PartnerApplyVo> selectList();



}
