package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.grade.po.UserGrade;
import com.zbkj.common.model.grade.dto.UserGradeDto;
import com.zbkj.common.model.grade.vo.UserGradeVo;

import java.util.List;

public interface UserGradeDao extends BaseMapper<UserGrade> {

    List<UserGradeVo> selectListByDto(UserGradeDto userGradeDto);

}
