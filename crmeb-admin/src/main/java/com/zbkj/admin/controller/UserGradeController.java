package com.zbkj.admin.controller;

import com.zbkj.common.model.extend.dto.PartnerApplyDto;
import com.zbkj.common.model.grade.dto.UserGradeDto;
import com.zbkj.common.model.grade.vo.UserGradeVo;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.token.FrontTokenComponent;
import com.zbkj.common.vo.LoginUserVo;
import com.zbkj.service.service.UserGradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Api(tags = "五级合伙人")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/admin/grade")
public class UserGradeController {

    private final UserGradeService userGradeService;

    /**
     * 五级合伙人列表
     * @param userGradeDto
     * @return
     */
    @PreAuthorize("hasAuthority('admin:grade:list')")
    @ApiOperation(value = "查询五级合伙人列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<List<UserGradeVo>> getList(UserGradeDto userGradeDto) {
        return CommonResult.success(userGradeService.selectListByDto(userGradeDto));
    }

    /**
     * 审批五级合伙人
     * @param id     * @return
     */
    @ApiOperation(value = "审批五级合伙人")
    @RequestMapping(value = "/approvePartner", method = RequestMethod.POST)
    public CommonResult<Boolean> approvePartner(@RequestParam("id") Long id, @RequestParam("state") Integer state) {
        boolean result = userGradeService.approvePartner(id, state);
        return CommonResult.success(result);
    }

    /**
     * 审批五级合伙人
     * @return
     */
    @ApiOperation(value = "获取五级合伙人申请列表")
    @RequestMapping(value = "/listApprovePartner", method = RequestMethod.GET)
    public CommonResult<CommonPage> listApprovePartner(@Validated PartnerApplyDto request,
                                                       @Validated PageParamRequest pageParamRequest) {
        CommonPage result = CommonPage.restPage(userGradeService.listApprovePartner(request, pageParamRequest));
        return CommonResult.success(result);
    }

}
