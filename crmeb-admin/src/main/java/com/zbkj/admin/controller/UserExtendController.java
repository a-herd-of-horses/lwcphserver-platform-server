package com.zbkj.admin.controller;

import com.zbkj.common.model.extend.dto.UserExtendDto;
import com.zbkj.common.model.extend.vo.UserExtendVo;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.UserExtendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "链动2+1")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/admin/extend")
public class UserExtendController {

    private final UserExtendService userExtendService;

    /**
     * 查询链动2+1列表
     * @param userExtendDto
     * @return
     */
    @PreAuthorize("hasAuthority('admin:extend:list')")
    @ApiOperation(value = "查询链动2+1列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<List<UserExtendVo>> getList(UserExtendDto userExtendDto) {
        return CommonResult.success(userExtendService.selectListByDto(userExtendDto));
    }

    /**
     * 删除链动2+1 和 五级合伙人
     * @param uid
     */
    @PreAuthorize("hasAuthority('admin:extend:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{uid}", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@PathVariable(value = "uid") Long uid) {
        userExtendService.removeTreeByUid(uid);
        return CommonResult.success();
    }

}
