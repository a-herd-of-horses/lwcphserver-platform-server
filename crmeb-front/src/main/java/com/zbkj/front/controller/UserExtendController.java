package com.zbkj.front.controller;

import com.zbkj.common.exception.ExceptionCodeEnum;
import com.zbkj.common.model.extend.dto.UserExtendDto;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.UserExtendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "链动2+1")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/front/extend")
public class UserExtendController {

    private final UserExtendService userExtendService;

    /**
     * 生成用户推广二维码
     * @param userExtendDto
     * @return
     */
    /*@ApiOperation(value = "生成用户推广二维码")
    @RequestMapping(value = "/code/create", method = RequestMethod.POST)
    public CommonResult<String> generateExtendCode(@RequestBody @Validated UserExtendDto userExtendDto) {
        return CommonResult.success(userExtendService.generateExtendCode(userExtendDto), ExceptionCodeEnum.SUCCESS.getMessage());
    }*/

    /**
     * 绑定上下级关系
     * @param userExtendDto
     * @return
     */
    @ApiOperation(value = "绑定上下级关系")
    @RequestMapping(value = "/binding", method = RequestMethod.POST)
    public CommonResult<String> bindingUser(@RequestBody @Validated UserExtendDto userExtendDto) {
        userExtendService.bindingUser(userExtendDto);
        return CommonResult.success();
    }

}
