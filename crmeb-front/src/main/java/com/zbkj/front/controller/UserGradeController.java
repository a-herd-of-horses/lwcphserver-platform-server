package com.zbkj.front.controller;

import com.zbkj.common.model.extend.dto.PartnerApplyDto;
import com.zbkj.common.model.extend.dto.UserExtendDto;
import com.zbkj.common.model.extend.vo.PartnerApplyVo;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.StoreProductSearchRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.token.FrontTokenComponent;
import com.zbkj.common.vo.LoginUserVo;
import com.zbkj.service.service.UserExtendService;
import com.zbkj.service.service.UserExtractService;
import com.zbkj.service.service.UserGradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Api(tags = "五级合伙人")
@RequiredArgsConstructor
@RestController
@RequestMapping("api/front/grade")
public class UserGradeController {

    private final UserGradeService userGradeService;

    @Resource
    private FrontTokenComponent tokenComponent;

    /**
     * 校验是否符合合伙人申请条件
     * @param id
     * @return
     */
    @ApiOperation(value = "校验是否符合合伙人申请条件")
    @RequestMapping(value = "/checkPartnerCondition/{id}", method = RequestMethod.GET)
    public CommonResult<Long> checkPartnerCondition(@PathVariable Integer id) {
        Long maxKey = userGradeService.checkPartnerCondition(id);
        return CommonResult.success(maxKey);
    }

    /**
     * 申请五级合伙人
     * @return
     */
    @ApiOperation(value = "申请五级合伙人")
    @RequestMapping(value = "/applyPartner", method = RequestMethod.POST)
    public CommonResult<Boolean> applyPartner(PartnerApplyDto partnerApplyDto, HttpServletRequest request) {
        Integer uid = tokenComponent.getLoginUser(request);
        partnerApplyDto.setApplicantId(Long.valueOf(uid));
        boolean result = userGradeService.applyPartner(partnerApplyDto);
        return CommonResult.success(result);
    }

    /**
     * 审批五级合伙人
     * @return
     */
    @ApiOperation(value = "获取五级合伙人申请列表")
    @RequestMapping(value = "/listApprovePartner", method = RequestMethod.GET)
    public CommonResult<CommonPage> listApprovePartner(@Validated PartnerApplyDto request,
                                                    @Validated PageParamRequest pageParamRequest) {
        CommonPage result = CommonPage.restPage(userGradeService.listApprovePartner(request, pageParamRequest));
        return CommonResult.success(result);
    }


}
