package com.zbkj.front.controller;

import com.zbkj.common.model.grade.po.OrderCity;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.OrderCityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/order/city")
@Api(tags = "五级区划")
public class OrderCityController {

    @Autowired
    private OrderCityService orderCityService;

    /**
     * 根据父级区号查询子级列表
     * @param pid
     * @return
     */
    @ApiOperation(value = "根据父级区号查询子级列表")
    @RequestMapping(value = "/listByPCode/{pid}", method = RequestMethod.GET)
    public CommonResult<List<OrderCity>> listByPCode(@PathVariable(value = "pid") String pid) {
        List<OrderCity> list = orderCityService.listByPCode(pid);
        return CommonResult.success(list);
    }

}
